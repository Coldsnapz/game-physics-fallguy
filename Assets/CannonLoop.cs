﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonLoop : MonoBehaviour
{
    [SerializeField] public GameObject Bullets = null;
    [SerializeField] public GameObject FirePoint = null;
    [SerializeField] public float BulletForce = 200;
    [SerializeField] public float StartDelay = 1;
    [SerializeField] public float Delay = 2;
    
    public float nextshot {get;set;}

    private void Start()
    {
        nextshot = Time.time + StartDelay;
    }

    private void Update()
    {
        if (Time.time > nextshot)
        {
            GameObject Bullet = Instantiate(Bullets, FirePoint.transform.position, Quaternion.identity);
            Bullet.GetComponent<MeshRenderer>().material = GetComponent<MeshRenderer>().material;

            Rigidbody rb = Bullet.GetComponent<Rigidbody>();
            rb.AddForce(transform.up * BulletForce, ForceMode.Impulse);
            nextshot = Time.time + Delay;
            Destroy(Bullet, 10);
        }
    }
}
