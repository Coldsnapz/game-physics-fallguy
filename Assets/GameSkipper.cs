﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSkipper : MonoBehaviour
{
    [Range(0, 10)]
    public float timeScale = 1;
    void Update()
    {
        Time.timeScale = timeScale;
    }
}
