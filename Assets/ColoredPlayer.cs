﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColoredPlayer : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.GetComponent<MeshRenderer>().material)
        {
            if (other.gameObject.GetComponent<MeshRenderer>() != null) { }
            Material c = other.gameObject.GetComponent<MeshRenderer>().material;
            SkinnedMeshRenderer[] m = this.gameObject.GetComponentsInChildren<SkinnedMeshRenderer>(true);
            foreach (SkinnedMeshRenderer g in m)
            {
                g.material = c;
            }
        }
    }

}
