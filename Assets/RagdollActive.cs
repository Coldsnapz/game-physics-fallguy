﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RagdollActive : MonoBehaviour
{
    public Collider MainCollider;
    public Collider[] AllColliders;

    public GameObject Tracker;
    public GameObject Playerbody;
    private bool Ragging;
    public GameObject HitEffect = null;
    public GameObject RespawnPos { get; set; }

    private void Awake()
    {
        MainCollider = GetComponent<Collider>();
        AllColliders = GetComponentsInChildren<Collider>(true);
        DoRagdoll(false);
        RespawnPos = new GameObject();
        RespawnPos.transform.position = this.transform.position;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            DoRagdoll(false);
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            DoRagdoll(true);
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            Reload();
        }
    }
    private void FixedUpdate()
    {
        if (Ragging)
        {
            Tracker.transform.position = Playerbody.transform.position;
        }
    }

    public void DoRagdoll(bool isRagdoll)
    {
        Ragging = isRagdoll;
        foreach (var col in AllColliders)
        {
            Rigidbody rb = col.gameObject.GetComponent<Rigidbody>();
            rb.velocity = Vector3.zero;
            col.enabled = isRagdoll;
            if (col.GetComponent<SkinnedMeshRenderer>() != null)
            {
                col.GetComponent<SkinnedMeshRenderer>().enabled = false;
                col.GetComponent<SkinnedMeshRenderer>().enabled = true;
            }

        }
        MainCollider.enabled = !isRagdoll;
        GetComponent<Rigidbody>().useGravity = !isRagdoll;
        GetComponent<Animator>().enabled = !isRagdoll;
    }

    public void Respawn()
    {
        transform.position = RespawnPos.transform.position;
        transform.rotation = RespawnPos.transform.rotation;
        foreach (var col in AllColliders)
        {
            col.gameObject.transform.position = RespawnPos.transform.position;
            col.gameObject.transform.rotation = RespawnPos.transform.rotation;
        }
        DoRagdoll(false);
        //Invoke("Reload",1);
    }
    public void Reload()
    {
        SceneManager.LoadScene("Stage");
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Bullets") || other.gameObject.CompareTag("Obstacle") || other.gameObject.CompareTag("KillZone"))
        {
            ContactPoint contact = other.contacts[0];
            Vector3 pos = contact.point;

            Instantiate(HitEffect, pos, Quaternion.identity);

            foreach (var col in AllColliders)
            {
                Rigidbody rb = col.gameObject.GetComponent<Rigidbody>();
                rb.velocity = Vector3.zero;
                rb.AddForce(other.gameObject.GetComponent<Rigidbody>().velocity * 1f + Vector3.up * 3, ForceMode.Impulse);
            }
            Invoke("Respawn", 2);
            DoRagdoll(true);
        }
    }
    private void OnCollisionStay(Collision other)
    {
        if (other.gameObject.CompareTag("KillZone"))
        {
            Invoke("Respawn", 2);
            DoRagdoll(true);
        }
    }
}
