﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    [SerializeField] public Transform _Checkpoint;
    [SerializeField] public ParticleSystem _Papers;
    [SerializeField] public ParticleSystem[] SmokeGenerators;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            _Papers.Play();
            foreach(ParticleSystem SG in SmokeGenerators)
            {
                SG.Play();
            }
            other.gameObject.GetComponent<RagdollActive>().RespawnPos.transform.position = _Checkpoint.position;
            Destroy(this.gameObject, 10);
        }
    }
}
