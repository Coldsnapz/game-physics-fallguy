﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour
{
    [SerializeField] public ParticleSystem[] particleGenerators;
    [SerializeField] public GameObject Wintext;
    public void Reload()
    {
        SceneManager.LoadScene("Stage");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Goal")
        {
            foreach (ParticleSystem ps in particleGenerators)
            {
                ps.Play();
            }
            Wintext.SetActive(true);
            Invoke("Reload", 10);
        }
    }
}
